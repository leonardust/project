package com.leonardust.suite.bankmanager.testcases;

import java.net.MalformedURLException;
import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.leonardust.datadriven.base.TestBase;
import com.leonardust.utilities.Constants;
import com.leonardust.utilities.DataProviders;
import com.leonardust.utilities.DataUtil;
import com.leonardust.utilities.ExcelReader;
import com.relevantcodes.extentreports.LogStatus;

public class AddCustomerTest extends TestBase{

	@Test(dataProviderClass=DataProviders.class, dataProvider="bankManagerDP")
	public void addCustomerTest(Hashtable<String, String> data) throws MalformedURLException {
		
		super.setUp();
		test = rep.startTest("Add Customer Test"+" "+data.get("browser"));
		setExtentTest(test);
		ExcelReader excel = new ExcelReader(Constants.SUITE1_EX_PATH);
		DataUtil.checkExecution("BankManagerSuite", "AddCustomerTest", data.get("Runmode"), excel);
		openBrowser(data.get("browser"));
		navigate("testurl");
		click("bmlBtn_CSS");
		click("addCustomerButton_CSS");
		type("firstname_CSS", data.get("firstname"));
		type("lastname_XPATH", data.get("lastname"));
		type("postalcode_CSS", data.get("postcode"));
		click("addButton_CSS");
		reportPass("Add Customer Test passed");
	}
	
	@AfterMethod
	public void tearDown()	{
		if(rep!=null) {
			rep.endTest(getExtTest());
			rep.flush();
		}
		getDriver().quit();
	}
	
}
