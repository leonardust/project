package com.leonardust.suite.bankmanager.testcases;

import java.net.MalformedURLException;
import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.leonardust.datadriven.base.TestBase;
import com.leonardust.utilities.Constants;
import com.leonardust.utilities.DataProviders;
import com.leonardust.utilities.DataUtil;
import com.leonardust.utilities.ExcelReader;
import com.relevantcodes.extentreports.LogStatus;

public class OpenAccountTest extends TestBase{

	@Test(dataProviderClass=DataProviders.class, dataProvider="bankManagerDP")
	public void openAccountTest(Hashtable<String, String> data) throws MalformedURLException {
		
		super.setUp();
		test = rep.startTest("Open Account Test"+" "+data.get("browser"));
		setExtentTest(test);
		ExcelReader excel = new ExcelReader(Constants.SUITE1_EX_PATH);
		DataUtil.checkExecution("BankManagerSuite", "OpenAccountTest", data.get("Runmode"), excel);
		openBrowser(data.get("browser"));
		navigate("testurl");
		click("bmlBtn_CSS");
		click("openAccountButton_CSS");
		select("customerName_CSS", data.get("customer"));
		select("currency_CSS", data.get("currency"));
		click("processButton_CSS");
		reportPass("Open Account Test passed");
	}
	
	@AfterMethod
	public void tearDown()	{
		if(rep!=null) {
			rep.endTest(getExtTest());
			rep.flush();
		}
		getDriver().quit();
	}
	
}
