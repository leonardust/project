package com.leonardust.utilities;

public class Constants {
	
	public static String TESTCASE_COL = "TestCase";
	public static String TESTCASE_SHEET = "TestCases";
	public static String SUITE_SHEET = "Suite";
	public static String DATA_SHEET = "TestData";
	public static String SUITENAME_COL = "SuiteName";
	public static String RUNMODE_COL = "Runmode";
	public static String RUNMODE_YES = "Y";
	public static String RUNMODE_NO = "N";
	public static String SUITESHEET_PATH = System.getProperty("user.dir") +"\\src\\test\\resources\\testdata\\Suite.xlsx";
	public static String SUITE1_EX_PATH = System.getProperty("user.dir") +"\\src\\test\\resources\\testdata\\BankManagerSuite.xlsx";
	public static String SUITE2_EX_PATH = System.getProperty("user.dir") +"\\src\\test\\resources\\testdata\\CustomerSuite.xlsx";

}

