package com.leonardust.testcases;

import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.leonardust.base.TestBase;

public class BankManagerLoginTest extends TestBase {

	@Test
	public void bankManagerLoginTest() throws InterruptedException, IOException {
		
		//verifyEquals("abc","xyz");
		
		log.debug("Inside Login test");
		click("bmlBtn_CSS");
		Assert.assertTrue(isElementPresent(By.cssSelector(OR.getProperty("addCustomerButton_CSS"))), "Login not successful");
		log.debug("Login successfully executed");
		
	}
}
