package com.leonardust.testcases;

import java.util.Hashtable;

import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;

import com.leonardust.base.TestBase;
import com.leonardust.utils.TestUtil;

public class OpenAccountTest extends TestBase{

	@Test(dataProviderClass=TestUtil.class, dataProvider="dp")
	public void openAccountTest(Hashtable<String,String> data) throws InterruptedException	{
		
		if(!data.get("runmode").equals("Y"))	{
			throw new SkipException("Skipping the test case as the Run mode is N");
		}
		
		click("openAccountButton_CSS");
		select("customerName_CSS", data.get("customer"));
		select("currency_CSS", data.get("currency"));
		click("processButton_CSS");
		
		Alert alert = wait.until(ExpectedConditions.alertIsPresent());
		alert.accept();
		}
	

}
