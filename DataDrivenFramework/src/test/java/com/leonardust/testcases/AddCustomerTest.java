package com.leonardust.testcases;

import java.util.Hashtable;

import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;

import com.leonardust.base.TestBase;
import com.leonardust.utils.TestUtil;

public class AddCustomerTest extends TestBase{

	@Test(dataProviderClass=TestUtil.class, dataProvider="dp")
	public void addCustomerTest(Hashtable<String,String> data) throws InterruptedException	{
		
		if(!data.get("runmode").equals("Y"))	{
			throw new SkipException("Skipping the test case as the Run mode is N");
		}
		
		click("addCustomerButton_CSS");
		type("firstname_CSS", data.get("firstname"));
		type("lastname_XPATH", data.get("lastname"));
		type("postalcode_CSS", data.get("postcode"));
		click("addButton_CSS");
		Alert alert = wait.until(ExpectedConditions.alertIsPresent());
		Assert.assertTrue(alert.getText().contains(data.get("alertext")));
		alert.accept();
	
		}
	
	
}
