package rough;

import com.leonardust.utilities.ExcelReader;
import com.leonardust.utilities.DataUtil;

public class CheckingRunmodes {

	public static void main(String[] args) {
		
		String suiteName = "BankManagerSuite";
		boolean suiteRunmode = DataUtil.isSuiteRunnable(suiteName);
		System.out.println(suiteRunmode);
		
		String testCaseName = "AddCustomerTest";
		boolean testCaseRunmode = DataUtil.isTestRunnable(testCaseName, new ExcelReader(System.getProperty("user.dir") + "\\src\\test\\resources\\testdata\\" + suiteName + ".xlsx"));
		System.out.println(testCaseRunmode);
	}

}
