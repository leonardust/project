package com.leonardust.suite.customer.testcases;

import java.util.Hashtable;
import org.testng.annotations.Test;

import com.leonardust.utilities.Constants;
import com.leonardust.utilities.DataProviders;
import com.leonardust.utilities.DataUtil;
import com.leonardust.utilities.ExcelReader;

public class OpenAccountTest {
	
	@Test(dataProviderClass=DataProviders.class, dataProvider="customerDP")
	public void openAccountTest(Hashtable<String, String> data) {
		
		ExcelReader excel = new ExcelReader(Constants.SUITE2_EX_PATH);
		DataUtil.checkExecution("CustomerSuite", "OpenAccountTest", data.get("Runmode"), excel);
	}
	
}
