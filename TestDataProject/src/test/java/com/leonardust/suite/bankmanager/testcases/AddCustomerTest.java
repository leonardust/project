package com.leonardust.suite.bankmanager.testcases;

import java.util.Hashtable;
import org.testng.annotations.Test;

import com.leonardust.utilities.Constants;
import com.leonardust.utilities.DataProviders;
import com.leonardust.utilities.DataUtil;
import com.leonardust.utilities.ExcelReader;

public class AddCustomerTest {
	
	@Test(dataProviderClass=DataProviders.class, dataProvider="bankManagerDP")
	public void addCustomerTest(Hashtable<String, String> data) {
		
		/*
		 * Suite
		 * TestCase
		 * Data
		 */
		
		ExcelReader excel = new ExcelReader(Constants.SUITE1_EX_PATH);
		DataUtil.checkExecution("BankManagerSuite", "AddCustomerTest", data.get("Runmode"), excel);
	}
	
}
