package com.leonardust.utilities;

import java.util.Hashtable;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestParametrization {
	
	@Test(dataProvider="getData")
	public void testData(Hashtable<String, String> data) {
		System.out.println(data.get("Runmode")+"---"+data.get("firstname")+"---"+data.get("lastname")+"---"+data.get("postcode"));
		//System.out.println(data.get("Runmode")+"---"+data.get("customer")+"---"+data.get("currency"));
	}

	@DataProvider
	public Object[][] getData()	{
		ExcelReader excel = new ExcelReader(System.getProperty("user.dir")+"\\src\\test\\resources\\testdata\\BankManagerSuite.xlsx");
		int rows = excel.getRowCount(Constants.DATA_SHEET);
		System.out.println("Total rows are: " + rows);
		
		String testName = "AddCustomerTest";
		// Find the test case start row
		int testCaseRowNum = 1;
		
		for(testCaseRowNum=1; testCaseRowNum<=rows; testCaseRowNum++) {
			String testCaseName = excel.getCellData(Constants.DATA_SHEET, 0, testCaseRowNum);
			if(testCaseName.equalsIgnoreCase(testName))	{
				break;
			}
		}
		System.out.println("Test case starts from row num: " + testCaseRowNum);	
		
		//Checking total rows in test case
		int dataStartRowNumber = testCaseRowNum + 2;
		int testRows = 0;
		while(!excel.getCellData(Constants.DATA_SHEET, 0, dataStartRowNumber + testRows).equals(""))	{
			testRows++;
		}
		System.out.println("Total rows of data are: " + testRows);
		
		//Checking total cols in test case
		int colStartColNumber = testCaseRowNum + 1;
		int testCols = 0;
		
		while(!excel.getCellData(Constants.DATA_SHEET, testCols, colStartColNumber).equals(""))	{
			testCols++;
		}
		System.out.println("Total cols of data are: " + testCols);
		
		//Printing data
		Object[][] data = new Object[testRows][1];
		int i = 0;
		for(int rNum = dataStartRowNumber; rNum<(dataStartRowNumber + testRows); rNum++)	{
			Hashtable<String, String> table = new Hashtable<String, String>();
			for(int cNum=0; cNum<testCols;cNum++)	{
				//System.out.println(excel.getCellData(Constants.DATA_SHEET, cNum, rNum));
				String testData = excel.getCellData(Constants.DATA_SHEET, cNum, rNum);
				String colName = excel.getCellData(Constants.DATA_SHEET, cNum, colStartColNumber);
				table.put(colName, testData);
			}
			data[i][0] = table;
			i++;
		}
		return data;
	}
	
}
